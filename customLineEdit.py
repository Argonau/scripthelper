"""
Custom LineEdit used in the script helper tool. Its an LineEdit that excepts files or folder drops.
It emits the file/folderpath as a string by a signal
"""
import os, sys
from pathlib import Path
from PyQt5 import QtCore, QtGui, QtWidgets

### for testing and display purposes: ###
import pprint
pp = pprint.PrettyPrinter(indent=4)
#########################################


class CustomLineEdit(QtWidgets.QLineEdit):
    """ gets a folder path via drag in a LineEdit widget""" 

    pathSignal = QtCore.pyqtSignal(str)

    def __init__(self, parent):
        super().__init__(parent)
        self.setToolTip('You can drop a folder from the explorer into this!')
        self.setClearButtonEnabled(True)
        self.setPlaceholderText("drop a file or folder in here!")
        self.setAcceptDrops(True)

        self.entered_Path = None
        self.displayFullPaths = True # set this to False to display only the file name        
        # self.displayFullPaths = False # set this to False to display only the file name

        self.accept_Files = True # set this to False if you want to catch only Folderpaths
        # self.accept_Files = False # set this to False if you want to catch only Folderpaths
        self.textEdited.connect(self.reset_path)

    def dragEnterEvent( self, event ):
        data = event.mimeData()
        urls = data.urls()
        if ( urls and urls[0].scheme() == 'file' ):
            event.acceptProposedAction()

    def dragMoveEvent( self, event ):
        data = event.mimeData()
        urls = data.urls()
        if ( urls and urls[0].scheme() == 'file' ):
            event.acceptProposedAction()

    def dropEvent( self, event ):
        data = event.mimeData()
        urls = data.urls()
        if ( urls and urls[0].scheme() == 'file' ):
            # for some reason, this doubles up the intro slash
            filepath = str(urls[0].path())[1:]

            if not self.accept_Files: # 
                if not os.path.isdir(filepath): # if a a file was droped and not a folder
                    filepath = Path(filepath).parent
            self.entered_Path = Path(filepath)

            if not self.displayFullPaths:
                self.setText(str(self.entered_Path.name))
            else:
                self.setText(str(self.entered_Path))

            self.pathSignal.emit(str(self.entered_Path))
    
    def switch_display(self):
        """slot for switching the lineEdit current text between fullpath and only filename display"""
        if self.entered_Path:
            if self.displayFullPaths:
                self.setText(str(self.entered_Path.name))
                self.displayFullPaths = False
            else:
                self.setText(str(self.entered_Path)) # emitting the path string, not the path object itself, like that I am able to emit a None also in case the lineEdit is empty
                self.displayFullPaths = True

    def reset_path(self):
        """slot for reseting 'self.entered_Path' when the lineEdit was cleared"""
        text = self.text()
        text = text.strip()

        if not text:
            self.entered_Path = None
            self.pathSignal.emit("")
            print("resetPath: ", self.entered_Path)

#### Demo ##############################################################################################################################
def main():
    """demonstrates the functionality"""


    app = QtWidgets.QApplication(sys.argv)

    window = QtWidgets.QWidget()
    window.resize(300, 60)
    window.move(800, 300)
    window.setWindowTitle('Widget Demo')

    myLineEdit = CustomLineEdit(None)

    button = QtWidgets.QPushButton(window)
    button.setText(f"Switch full path display")
    button.setMinimumSize(QtCore.QSize(60, 30))
    button.clicked.connect(myLineEdit.switch_display)

    window_layout = QtWidgets.QVBoxLayout(window) # verticalLayout
    window_layout.addWidget(myLineEdit)
    window_layout.addWidget(button)
    window.setLayout(window_layout)
    window.show()
    app.exec_()


if __name__ == '__main__':
    main()