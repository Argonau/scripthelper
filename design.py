# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'design/design_script-helper.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(496, 192)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/favicon.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setObjectName("groupBox")
        self.gridLayout = QtWidgets.QGridLayout(self.groupBox)
        self.gridLayout.setObjectName("gridLayout")
        self.lineEdit_shell = CustomLineEdit(self.groupBox)
        self.lineEdit_shell.setObjectName("lineEdit_shell")
        self.gridLayout.addWidget(self.lineEdit_shell, 0, 0, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 2, 3, 1, 1)
        self.pushButton_shell = QtWidgets.QPushButton(self.groupBox)
        self.pushButton_shell.setMinimumSize(QtCore.QSize(130, 50))
        self.pushButton_shell.setObjectName("pushButton_shell")
        self.gridLayout.addWidget(self.pushButton_shell, 2, 0, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem1, 2, 1, 1, 1)
        self.lineEdit_convertDesign = CustomLineEdit(self.groupBox)
        self.lineEdit_convertDesign.setObjectName("lineEdit_convertDesign")
        self.gridLayout.addWidget(self.lineEdit_convertDesign, 0, 2, 1, 1)
        self.pushButton_build = QtWidgets.QPushButton(self.groupBox)
        self.pushButton_build.setMinimumSize(QtCore.QSize(130, 50))
        self.pushButton_build.setObjectName("pushButton_build")
        self.gridLayout.addWidget(self.pushButton_build, 2, 4, 1, 1)
        self.pushButton_convertDesign = QtWidgets.QPushButton(self.groupBox)
        self.pushButton_convertDesign.setMinimumSize(QtCore.QSize(130, 50))
        self.pushButton_convertDesign.setObjectName("pushButton_convertDesign")
        self.gridLayout.addWidget(self.pushButton_convertDesign, 2, 2, 1, 1)
        self.lineEdit_build = CustomLineEdit(self.groupBox)
        self.lineEdit_build.setObjectName("lineEdit_build")
        self.gridLayout.addWidget(self.lineEdit_build, 0, 4, 1, 1)
        self.lineEdit_convertResource = CustomLineEdit(self.groupBox)
        self.lineEdit_convertResource.setText("")
        self.lineEdit_convertResource.setObjectName("lineEdit_convertResource")
        self.gridLayout.addWidget(self.lineEdit_convertResource, 1, 2, 1, 1)
        self.verticalLayout.addWidget(self.groupBox)
        self.label_projectPath = QtWidgets.QLabel(self.centralwidget)
        self.label_projectPath.setObjectName("label_projectPath")
        self.verticalLayout.addWidget(self.label_projectPath)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Script helper tool"))
        self.groupBox.setTitle(_translate("MainWindow", "Scripts"))
        self.lineEdit_shell.setPlaceholderText(_translate("MainWindow", "lineEdit_shell"))
        self.pushButton_shell.setText(_translate("MainWindow", "Run in Shell"))
        self.lineEdit_convertDesign.setPlaceholderText(_translate("MainWindow", "lineEdit_convertDesign"))
        self.pushButton_build.setText(_translate("MainWindow", "Build system"))
        self.pushButton_convertDesign.setText(_translate("MainWindow", "Convert design files"))
        self.lineEdit_build.setPlaceholderText(_translate("MainWindow", "lineEdit_build"))
        self.lineEdit_convertResource.setPlaceholderText(_translate("MainWindow", "lineEdit_convertResource"))
        self.label_projectPath.setText(_translate("MainWindow", "label_projectPath"))

from customLineEdit import CustomLineEdit
import designResources_rc
