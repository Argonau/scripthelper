"""Script helper: converts Ui files and the resource files from QT Designer to pyqt python files
"""

import subprocess
import os

command_UI = 'pyuic5.exe "design/design_script-helper.ui" -o "design.py"' # double quotation marks make this work when there is whitespaces in the path!!! 
command_Resources = 'pyrcc5 "resources/designResources.qrc" -o "designResources_rc.py" ' # the resource python file has to be called [yourResourceSomething]_rc.py or it won't work
cwd = os.getcwd()

subprocess.call(command_UI, shell=False, cwd=cwd)
subprocess.call(command_Resources, shell=False, cwd=cwd)

print("done")