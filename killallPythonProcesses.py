# -*- coding: utf-8 -*-

import os

def killAllPythons():
	"""This thing kills all python processes"""
	os.system("taskkill /f /im python.exe")
	# /f : Specifies that process(es) be forcefully terminated.
	# /im (ImageName ): Specifies the image name of the process to be terminated.

def killOwnProcess():
	"""This thing kills only the own process started"""
	os.kill(os.getpid(), 9)

def killByPID(pid):
	"""This thing kills only the own process started"""
	try:
		os.kill(pid, 9) # same as saying: "import signal" and "os.kill(pid, signal.SIGTERM)"
		print(f"PID: {pid} got killed.")
	except (PermissionError, OSError): # PermissionError when the process is done but still locked, OSError happens when the PID is not existing
		print(f"Process with ID {pid} has finished or is not existing.")
	except TypeError:
		print("Got no PID Integer")

def killAllCoversions():
	"""This thing kills all python processes"""
	os.system("taskkill /f /im AliasToIges.exe")
	os.system("taskkill /f /im Catia5ToAlias.exe")
	
	# /f : Specifies that process(es) be forcefully terminated.
	# /im (ImageName ): Specifies the image name of the process to be terminated.

# print(os.getpid())
def killAllVreds():
	"""This thing kills all vred processes"""
	os.system("taskkill /f /im VREDPRO.exe")

killAllCoversions()

killAllPythons()
# killByPID(None)