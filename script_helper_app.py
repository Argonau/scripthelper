"""Script helper tool
"""

__author__ = "arwed.gollner"
__version__ = "1.0.0"  # 2018.10.24

from PyQt5 import QtWidgets, QtCore
import os, sys# We need sys so that we can pass argv to QApplication
import subprocess
import pathlib
import design # This file holds our MainWindow and all design related things
              # it also keeps events etc that we defined in Qt Designer

import time

class AppClass(QtWidgets.QMainWindow, design.Ui_MainWindow):

    version = __version__
    formats = (".igs", ".CATPart", ".stp", ".edf", ".prt", ".fbx", ".wref", ".wire", ".vpb")    # FIXME: obj, dwg geht nicht

    def __init__(self):
        super().__init__()

        ### setup UI ###
        self.setupUi(self)  # This is defined in design.py file automatically. It sets up layout and widgets that are defined
        self.startUpUIChanges()
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)

        self.workThread_shell = WorkThread(self, self.run_in_shell)
        self.workThread_design = WorkThread(self, self.convert_design_files)
        self.workThread_resource = WorkThread(self, self.convert_resource_files)
        ### some default start values ###
        # self.listWidget_rn_rename.formats = self.formats # changing the customlistWidget formats to the ones used in this app

        # ### set up a custom Tab ###
        # ### don't forget the set a placeholder for the custom class in Qt designer in the design.ui file. ###
        # ### tab_function2.py is used to put all the methods, slots from a specific tab into a separate file
        # self.tab_function2.appObj = self # !!!! this is necessary to give back the app object (in our case called "form"). Now all widgets get accessable in that Custom Tab class
        # self.tab_function2.startup_UIChanges()
        # self.button_f2_start.clicked.connect(self.tab_function2.change_color)

        # self.pushButton_shell.clicked.connect(self.run_in_shell)
        self.pushButton_shell.clicked.connect(self.workThread_shell.start)
        self.pushButton_convertDesign.clicked.connect(self.workThread_design.start)
        self.pushButton_convertDesign.clicked.connect(self.workThread_resource.start)

        print("children: ", self.children())

    def startUpUIChanges(self):
        _translate = QtCore.QCoreApplication.translate
        self.mainTitle = "QT App dev helper " + self.version
        self.setWindowTitle(_translate("MainWindow", self.mainTitle))

        self.lineEdit_shell.setPlaceholderText("python script to run")
        self.label_projectPath.setText("...")

    def testfunction(self):
        print("wait")
        time.sleep(10)
        print("some Attribute:", self.version)

    def run_in_shell(self):
        """runs a given script in a new Powershell window"""
        aPath = pathlib.Path(self.lineEdit_shell.text()) # FIXME: Rather self.lineEdit_shell.entered_Path and build a method to clear that path if there is no text in the lineEdit
        print("currentText: ", aPath)

        if aPath.exists() and aPath.match('*.py'):
            script_file = aPath.name
            subprocess.call(["powershell", "-noexit", r"python " + str(script_file)], cwd=aPath.parent, creationflags=subprocess.CREATE_NEW_CONSOLE)

    def convert_design_files(self):
        """converts .ui files to python design files"""
        if self.lineEdit_convertDesign.entered_Path:
            aPath = pathlib.Path(self.lineEdit_convertDesign.entered_Path) # FIXME: Build a method to clear that path if there is no text in the lineEdit
        else: return None

        designFile = "design.py"

        if aPath.exists() and aPath.match('*.ui'):
            uiDesignFile = aPath.name
            uiDesignPython = aPath.parents[1].joinpath("design.py") # '.parents' :An immutable sequence providing access to the logical ancestors of the path
            cwd = aPath.parent
            command_UI = f'pyuic5.exe "{aPath}" -o "{uiDesignPython}"' # eg: "pyuic5.exe design/design_script-helper.ui -o design.py"
            # command_Resources = "pyrcc5 resources/DesignRessources.qrc -o DesignRessources_rc.py"
            # print("81: ", command_Resources)
            cwd = aPath.parents[1]

            subprocess.call(command_UI, shell=False, cwd=cwd)
            # subprocess.call(command_Resources, shell=False, cwd=cwd)

            print("Design done")

    def convert_resource_files(self):
        """converts design resources files to python design files"""
        if self.lineEdit_convertResource.entered_Path:
            aPath = pathlib.Path(self.lineEdit_convertResource.entered_Path) # FIXME: Build a method to clear that path if there is no text in the lineEdit
        else: return None

        designFile = "designRessources.py"

        if aPath.exists() and aPath.match('*.qrc'):
            uiDesignFile = aPath.name
            uiDesignPython = aPath.parents[1].joinpath("designResources_rc.py") # '.parents' :An immutable sequence providing access to the logical ancestors of the path
            cwd = aPath.parent
            # command_UI = f'pyuic5.exe "{aPath}" -o "{uiDesignPython}"' # eg: "pyuic5.exe design/design_script-helper.ui -o design.py"
            command_Resources = f'pyrcc5 "{aPath}" -o "{uiDesignPython}"' # eg: "pyuic5.exe design/design_script-helper.ui -o design.py"
            # command_Resources = "pyrcc5 resources/DesignRessources.qrc -o DesignRessources_rc.py"
            print("81: ", command_Resources)
            cwd = aPath.parents[1]

            # subprocess.call(command_UI, shell=False, cwd=cwd)
            subprocess.call(command_Resources, shell=False, cwd=cwd)

            print("Resources done")


class WorkThread(QtCore.QThread):

    def __init__(self, appObj, function, *args, **kwargs):  # appClass Objectname muss noch übergeben werden
        super().__init__()

        self.is_running = False
        self.appObj = appObj
        self.function = function
        self.args = args
        self.kwargs = kwargs
        # self.setRunState(False)  # emits the signal that "self.is_running" is False

    def signalTest(self, irgendwas):  # Funktioniert aber warum kann ich einfach "irgendwas als Argument schreiben und er zeigt die richtige Liste an?"
        """prints the signal"""
        print(irgendwas)

    def run(self):
        """runs the thread"""
        # self.setRunState(True)
        self.function()
        print(f"workthread with {self.function} is running")

    def stop(self):
        """stops the thread"""
        # self.setRunState(False)
        # self.terminate()  # stopping the thread # FIXME: this is suppost to be bad. rather use stop
        super().stop()
        print("stopping thread...")

def main():
    app = QtWidgets.QApplication(sys.argv)  # A new instance of QApplication
    form = AppClass()                 # We set the form to be our App (design)
    # form.move(1900, 50) # FIXME: make this generic for different screensizes
    form.show()                         # Show the form
    app.exec_()                         # and execute the app


if __name__ == '__main__':              # if we're running file directly and not importing it
    main()                              # run the main function
