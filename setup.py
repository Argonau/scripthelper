
# the setup.py for the AutoFileConverter-Tool:
# -----------------------------------------------------------------------
from cx_Freeze import setup, Executable
from zee_app import __version__ as appVersion
import sys

# buildOptions = dict(include_files = ['Baseskript.py', 'igesimport.def', 'step214ug.def'], excludes ["tkinter"])
# buildOptions = dict(excludes = ["tkinter"])
buildOptions = {"excludes": ["tkinter", "PyQt5.QtSql", "sqlite3",  # Wenn Tkinter nicht exkluded wird und im skript verwendet wird, crashed das erzeugen der Exe
                            	"scipy.lib.lapack.flapack",
                                "PyQt5.QtNetwork",
                                "PyQt5.QtScript",
                                "numpy.core._dotblas", 
                                "PyQt4"],
                "include_files" : [r'settings/'], # folder, relative path
                "optimize": 2,
                "zip_include_packages": "PyQt5"		# das Ding hat viel gebracht!!! 216 Mb -> 60 Mb
                }

if sys.platform == "win32":
    base = "Win32GUI" # base so cmd wont open, Win32GUI This doesn’t use a console window, and reports errors in a dialog box.
else:
    base = None

target = Executable(
    script="zee_app.py",
    base=base, # base so cmd wont open
    targetName= "appTemplate.exe",  # targetName is the filename of the executable it's going to produce. On Windows, executables must have a ".exe" extension
    icon="resources/favicon.ico",
    )

setup(
	name = "Application Template",
	version = appVersion,  # version = "1.0.4",
	description = "This is a good start point to create a desktop application",
	author = "Arwed Gollner",
	options = {"build_exe": buildOptions},
	executables = [target], 
	)
# ----------------------------------------------------------------------------
# The command line: c:\python32\python.exe setup.py build
# NOTE: first go to the python32 folder in command prompt. then use the command above.