"""run the python script in a powershell window"""

import subprocess
import os

cwd = os.path.dirname(os.path.realpath(__file__)) # path of the folder where this file was started from
subprocess.call(["powershell", "-noexit", r"python script_helper_app.py"], cwd=cwd, creationflags=subprocess.CREATE_NEW_CONSOLE)
# subprocess.call(["powershell", "-noexit", r"python customlistwidget.py"], cwd=cwd, creationflags=subprocess.CREATE_NEW_CONSOLE)
