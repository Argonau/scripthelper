"""Custom Tab widget
"""

import os, sys
from pathlib import Path
from PyQt5 import QtCore, QtGui, QtWidgets

import random

### for testing and display purposes: ###
import pprint
pp = pprint.PrettyPrinter(indent=4)
#########################################

class Tab_function2(QtWidgets.QTabWidget):
	"""external class that holds all the methods and slots that are related to this tab"""

	def __init__(self):
		super().__init__()
		self.appObj = None  # in the main application this one has to be set to "self".  E.g.: "self.tab_function2.appObj = self""
							# we have then access to the UI elements.

	def startup_UIChanges(self):
		"""adjust this tabs UI at start up. Don't put in the '__init__' thought since self.appObj is still 'None' """
		self.appObj.frame_f2.setStyleSheet("#frame_f2 {background: #8A2BE2;}")

	def signaltest(self, event):
		"""pretty self explaining, isnt it?"""
		print("Tab_function2 class: ", event)

	def change_color(self):
		"""change the background of 'frame_f2' Qframe object to a random color"""
		
		red, green, blue = random.randrange(0, 255), random.randrange(0, 255), random.randrange(0, 255)
		colorString = "{" + f"background: rgb({red}, {green}, {blue})" + " ;}"
		self.appObj.frame_f2.setStyleSheet(f"#frame_f2 {colorString}") # random color

